import React, { useState, useEffect } from "react";

import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

/* Functional component that renders a form to add or edit task */
export default function Form(props) {
  const [id, setid] = useState("");
  const [name, setname] = useState("");
  const [description, setdescription] = useState("");
  const [estimate, setestimate] = useState("");
  const [status, setstatus] = useState("Planned");
  const [errorestimate, setErrorestimate] = React.useState(false);
  const [errorName, setErrorName] = React.useState(false);
  const [errordescription, setErrordescription] = React.useState(false);
  const [edition, setEdition] = React.useState(false);

  const classes = useStyles();

  useEffect(() => {
    const task = props.task;
    //If task is passed, set values and edition to true
    if (Object.keys(task).length > 0) {
      setEdition(true);
      setid(task.id);
      setname(task.name);
      setdescription(task.description);
      setestimate(task.estimate);
      setstatus(task.state);
    } else {
      setEdition(false);
    }
  }, []);

  const handleChangeText = (event) => {
    switch (event.target.name) {
      case "name":
        setname(event.target.value);
        setErrorName(false);
        break;
      case "description":
        setdescription(event.target.value);
        setErrordescription(false);
        break;
      case "estimate":
        setestimate(event.target.value);
        setErrorestimate(false);
        break;
      case "status":
        setstatus(event.target.value);
        break;
    }
  };
  //Function that validates input
  //Post:returns true if there is an error , false otherwise
  const checkValidInput = () => {
    let error = false;
    if (name.length < 1) {
      setErrorName(true);
      error = true;
    }
    if (isNaN(estimate) || estimate <= 0) {
      setErrorestimate(true);
      error = true;
    }
    return error;
  };
  const handleSave = () => {
    if (checkValidInput()) {
      alert("Please check the marked box");
    } else {
      const task = { name: name, description: description, estimate: estimate, state: status };
      if (!edition) {
        props.handleAdd(task);
      } else {
        props.handleEdit(id, task);
      }
      props.handleCancel(); //go back to initial screen
    }
  };

  return (
    <Container>
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          {edition ? "Edit task" : "New task"}
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <TextField
                name="name"
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Name"
                autoFocus
                value={name}
                onChange={handleChangeText}
                error={errorName}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="description"
                label="Description"
                name="description"
                autoComplete="lname"
                value={description}
                onChange={handleChangeText}
                error={errordescription}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="estimate"
                label="estimate in hours"
                type="number"
                name="estimate"
                autoComplete="estimate"
                value={estimate}
                onChange={handleChangeText}
                error={errorestimate}
              />
            </Grid>
            <Grid item xs={6}>
              <Select value={status} style={{ width: "100%", height: "100%" }} label="Status" onChange={(e) => setstatus(e.target.value)}>
                <MenuItem value={"Planned"}>Planned</MenuItem>
                <MenuItem value={"In progress"}>In progress</MenuItem>
                <MenuItem value={"Completed"}>Completed</MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Button fullWidth variant="contained" color="primary" className={classes.submit} onClick={handleSave}>
            {edition ? "Edit task" : "Add task"}
          </Button>
          <Button fullWidth variant="contained" color="secondary" className={classes.submit} onClick={props.handleCancel}>
            Cancel
          </Button>
        </form>
      </div>
    </Container>
  );
}
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginLeft: theme.spacing(10)
  },
  form: {
    width: "50%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 300
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));
