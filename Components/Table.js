import React from "react";
import styles from "./Table.module.css";
import BarGraph from "./BarGraph";

/*Render Table with add, edit a delete buttons
props: title, data(array), handleAdd, handleDelete, handleEdit
*/
export default function Table(props) {
  const { title, data, handleAdd, handleDelete, handleEdit } = props;
  const [renderStats, setrenderStats] = React.useState(false);
  const deleteTask = (id) => {
    const response = confirm("Are your sure you want to delete task id " + id);
    console.log("respuesta", response);
    if (response) {
      handleDelete(id);
    }
  };
  const editTask = (id) => {
    const response = confirm("Are your sure you want to edit task id " + id);
    if (response) {
      handleEdit(id);
    }
  };
  const showStats = () => {
    setrenderStats(true);
  };
  const goBack = () => {
    setrenderStats(false);
  };
  return (
    <>
      {renderStats ? (
        <BarGraph tasks={data} handleBack={goBack} />
      ) : (
        <div className={styles.tablecontainer}>
          <h1 className={styles.title}> {title}</h1>
          <a onClick={handleAdd} className={styles.addbutton}>
            Add task
          </a>
          <a onClick={showStats} className={styles.addbutton}>
            Show Stats
          </a>
          <table>
            <tr>
              <th>id</th>
              <th>Name</th>
              <th>Description</th>
              <th>Estimate</th>
              <th>state</th>
              <th>Action</th>
            </tr>

            {data.length > 1 &&
              data.map((task, index) => {
                const { id, name, description, estimate, state } = task;
                return (
                  <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{description}</td>
                    <td>{estimate}</td>
                    <td>{state}</td>
                    <td>
                      <a onClick={() => editTask(id)} className={styles.editbutton}>
                        Edit
                      </a>
                      {"         "}
                      <a onClick={() => deleteTask(id)} className={styles.deletebutton}>
                        Delete
                      </a>
                    </td>
                  </tr>
                );
              })}
          </table>
        </div>
      )}
    </>
  );
}
