import React from "react";
import { Bar } from "react-chartjs-2";
import Button from "@material-ui/core/Button";

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};
/* Function that renders a bar graph of the acumulated hour by task´s state 
props: tasks(array) , handleBack (callback)*/
const VerticalBar = ({ handleBack, tasks }) => {
  const [data, setData] = React.useState({});

  React.useEffect(() => {
    const len = tasks.length;
    if (len > 0) {
      let totalPlaned = 0;
      let totalInProg = 0;
      let totalCompl = 0;
      for (let i = 0; i < len; i++) {
        if (tasks[i].state == "Planned") {
          totalPlaned += tasks[i].estimate;
        } else if (tasks[i].state == "In progress") {
          totalInProg += tasks[i].estimate;
        } else if (tasks[i].state == "Completed") {
          totalCompl += tasks[i].estimate;
        }
      }
      setData({
        labels: ["Planned", "In progress", "Completed"],
        datasets: [
          {
            label: "Total hours of tasks",
            data: [totalPlaned, totalInProg, totalCompl],
            backgroundColor: ["rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(255, 206, 86, 0.2)"],
            borderColor: ["rgba(255, 99, 132, 1)", "rgba(54, 162, 235, 1)", "rgba(255, 206, 86, 1)"],
            borderWidth: 1
          }
        ]
      });
    }
  }, []);
  return (
    <div style={{ display: "flex", width: "80%", marginLeft: "auto", marginRight: "auto", flexDirection: "column" }}>
      <div className="header">
        <h1 className="title">Acumulated hours by task's state</h1>
      </div>

      <Bar data={data} options={options} />
      <Button fullWidth variant="contained" color="secondary" onClick={handleBack}>
        Go Back
      </Button>
    </div>
  );
};

export default VerticalBar;
