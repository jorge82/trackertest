
# Exercise: Task tracker   
Please develop a SPA with these simple requirements:   
* Present visually a list of tasks in 3 different states: Planned, In progress, Completed.   
* Each task contains, name, description, estimate and state.   
* The user needs to be able to add/remove and change the state of the tasks.  
* Somewhere in the application you need to present an up to date status, adding up all hours for each state.  
Notes:   
You can assume it's a single user application, and there's no need to store data; but the user needs to be able to add/remove and see tasks.  

Mocking a REST api is suggested.   
You can reuse any existing library, but please distribute the solution with them.   
Write code as you would for production, having in mind that usability is important as well.   

Use React JS, React Native or Angular 2+


# Install
Install commands:
    npm install -g json-server
    npm install
# Run 
Start mock server in port 3002:
    json-server --watch db.json --port 3002
Start react in port 3000:
    npm run dev
      
# Project structure
    -Components
        -BarGraph.js
        -Form.js
        -Table.js
    -Pages
        -index.js
    -db.json --->db mock api       

# Disclaimer
    No automatic testing was done

