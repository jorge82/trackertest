import React, { useState, useEffect } from "react";
import Head from "next/head";
import Table from "../Components/Table";
import Form from "../Components/Form";
import axios from "axios";

const APIURL = "http://localhost:3002/tasks";

/*Functional component that renders the table with task or
the form for adding o deleting task */
export default function Home() {
  const [renderForm, setrenderForm] = useState(false);

  const [tasks, setTasks] = useState([]);
  //currentTask is used for edition
  const [currentTask, setcurrentTask] = useState({});

  //Loading task from backend
  useEffect(() => {
    axios
      .get(APIURL)
      .then((response) => {
        setTasks(response.data);
      })
      .catch((e) => {
        console.error("Conection error", e);
        alert("Connection error!");
      });
  }, []);

  const saveTask = (task) => {
    axios
      .post(APIURL, { ...task })
      .then((response) => {
        console.log("response", response.data);
        setTasks((oldtasks) => [...oldtasks, response.data]);
      })
      .catch((e) => {
        console.error("Error", e);
        alert("Adding error!");
      });
  };
  const deleteTask = (idToDelete) => {
    axios
      .delete(APIURL + "/" + idToDelete)
      .then((response) => {
        setTasks(tasks.filter((task) => task.id != idToDelete));
      })
      .catch((e) => {
        console.error("Error", e);
        alert("Deleting error!");
      });
  };
  const editTask = (id, task) => {
    axios
      .put(APIURL + "/" + id, { ...task })
      .then((response) => {
        const newTasks = tasks.map((oldTask) => {
          if (oldTask.id == id) {
            task.id = id;
            return task;
          } else {
            return oldTask;
          }
        });
        setTasks(newTasks);
      })
      .catch((e) => {
        console.error("Error", e);
        alert("updating error!");
      });
  };
  //function that set task for edition
  const handleEdit = (id) => {
    setcurrentTask(tasks.filter((task) => task.id == id)[0]);
    setrenderForm(true);
  };

  const handleAdd = (task) => {
    setcurrentTask({});
    setrenderForm(true);
  };
  const goBack = () => {
    setrenderForm(false);
  };

  return (
    <div>
      <Head>
        <title>Task tracker</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {renderForm ? (
        <Form task={currentTask} handleAdd={saveTask} handleCancel={goBack} handleEdit={editTask} />
      ) : (
        <div>
          <Table title={"Task tracker"} data={tasks} handleAdd={handleAdd} handleEdit={handleEdit} handleDelete={deleteTask} />
        </div>
      )}
    </div>
  );
}
